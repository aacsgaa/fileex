package com.netty.websocket;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelPromise;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame;
import io.netty.util.concurrent.GlobalEventExecutor;

import java.util.HashMap;

public final class Global {

    public static ChannelGroup group = new DefaultChannelGroup(GlobalEventExecutor.INSTANCE);

    public static HashMap<String, Channel> userChannel=new HashMap<String, Channel>();

    public static final String UPLOAD_TYPE="1";

    public static final String DOWNLOAD_TYPE="2";

    public static void push(String user,HashMap data,String type,boolean isFinish)throws Exception{
        Channel webSocketUserChannel = userChannel.get(user);
        HashMap map=new HashMap();
        map.put("type",type);
        map.put("finish",isFinish);
        map.put("data",data);
        ObjectMapper objectMapper=new ObjectMapper();
        String json = objectMapper.writeValueAsString(map);
        webSocketUserChannel.writeAndFlush(new TextWebSocketFrame(json));
    }


}   